# Vault Service Mock

## Dev Setup

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd models
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

## Code Style

```bash
isort .
black .
pycodestyle vault_service_mock
pylint vault_service_mock
```

## Tests

```bash
pytest
```
