# 0.2.1

* api url is now `v0` like real Vault (was `v1` before)

# 0.2.0

* removed wrapping of configuration response in sub dictionary

# 0.1.2

* moved from EATS to this public repo
