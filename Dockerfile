# build stage
FROM docker.io/ubuntu:20.04 AS builder
RUN apt-get update \
&& apt-get install -y python3-venv python3-pip git curl

# service name
RUN apt-get install -y git curl
ENV PATH /root/.local/bin:/root/.poetry/bin:${PATH}
RUN mkdir -p /root/.local/bin \
&& ln -s $(which python3) /root/.local/bin/python \
&& curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python
COPY . /root/app
WORKDIR /root/app
RUN poetry export -f requirements.txt > requirements.txt
RUN poetry build

# install stage
FROM docker.io/ubuntu:20.04
RUN apt-get update \
&& apt-get install -y python3-venv python3-pip git curl

ENV VIRTUAL_ENV /opt/app/venv
ENV PATH ${VIRTUAL_ENV}/bin:${PATH}
RUN python3 -m venv $VIRTUAL_ENV \
&& pip3 install wheel \
&& pip3 install uvicorn
COPY --from=builder /root/app/requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt
COPY --from=builder /root/app/dist/*.whl /tmp
RUN pip3 install /tmp/*.whl

EXPOSE 80/tcp
ENTRYPOINT ["uvicorn", "vault_service_mock.main:app"]
CMD ["--host", "0.0.0.0", "--port", "80"]
