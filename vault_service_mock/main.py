import logging
import os

from vault_service_mock.app import create_app
from vault_service_mock.mongo_client import MongoClient

logger = logging.getLogger("uvicorn")
mongo_url = os.getenv("MONGO_URL", "mongodb://localhost:27017")
db_name = os.getenv("MONGO_DATABASE", "eats")

app = create_app(MongoClient(mongo_url, db_name))
