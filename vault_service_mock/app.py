from fastapi import FastAPI
from starlette import status

from vault_service_mock import __version__, api


def create_app(mongo_client):
    app = FastAPI(title="Vault Service Mock", version=__version__)

    @app.on_event("startup")
    async def _():
        await mongo_client.initialize()

    app.include_router(api.create_router(mongo_client), prefix="/api/v0")

    @app.get("/alive", status_code=status.HTTP_200_OK)
    def _():
        return {"status": "ok"}

    return app
