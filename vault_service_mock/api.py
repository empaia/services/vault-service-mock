from typing import Dict, Union

from fastapi import APIRouter, Body, Path
from pydantic import StrictBool, StrictFloat, StrictInt, StrictStr

ConfigurationModel = Dict[StrictStr, Union[StrictStr, StrictInt, StrictFloat, StrictBool]]


def create_router(mongo_client) -> APIRouter:
    router = APIRouter()

    @router.get(
        "/admin/{app_id}",
        summary="Get App configuration",
        response_model=ConfigurationModel,
        responses={
            200: {
                "description": "The configuration object for the App with the given id",
                "content": {
                    "application/json": {
                        "example": {
                            "some_token": "secret-token",
                            "some_flag": True,
                            "some_param": 42,
                            "some_threshold": 0.5,
                        }
                    }
                },
            }
        },
    )
    async def _(app_id: str = Path(None, example="29630c57-a318-43cd-ad0a-00dcbaa3055c", description="id of the App")):
        """
        Queries the configuration for the App with the given id.
        """
        return await mongo_client.get_configuration(app_id)

    @router.post("/admin/{app_id}", summary="Create configuration for the App with the given id")
    async def _(
        app_id: str = Path(None, example="29630c57-a318-43cd-ad0a-00dcbaa3055c", description="id of the App"),
        configuration: ConfigurationModel = Body(..., description="The App configuration"),
    ):
        """
        Creates a configuration for the App with the given id.
        """
        await mongo_client.add_configuration(app_id, configuration)

    return router
