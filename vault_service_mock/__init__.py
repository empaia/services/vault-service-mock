from importlib.metadata import version

__version__ = version("vault-service-mock")
