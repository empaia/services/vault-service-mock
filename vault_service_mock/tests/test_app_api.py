from fastapi import status
from fastapi.testclient import TestClient

from vault_service_mock.app import create_app


class MongoClientMock:
    def __init__(self):
        self.configurations = dict()

    async def initialize(self):
        pass

    async def add_configuration(self, app_id, configuration):
        self.configurations[app_id] = configuration

    async def get_configuration(self, app_id):
        return self.configurations[app_id]


mongo_client_mock = MongoClientMock()
app_client = TestClient(create_app(mongo_client_mock))

test_configuration = dict(some_token="very-secret", some_param=42, some_flag=True, some_threshold=0.5)


class TestAppApi:
    def test_post_configuration(self):
        response = app_client.post("/api/v0/admin/some-app-id", json=test_configuration)
        assert response.status_code == status.HTTP_200_OK
        assert test_configuration == mongo_client_mock.configurations["some-app-id"]

    def test_get_configuration(self):
        mongo_client_mock.configurations["some-app-id"] = test_configuration
        response = app_client.get("/api/v0/admin/some-app-id")
        assert response.json() == test_configuration
