from fastapi import HTTPException, status
from motor import motor_asyncio


class MongoClient:
    def __init__(self, mongo_url, database_name):
        self._client = motor_asyncio.AsyncIOMotorClient(mongo_url)
        self._db_name = database_name

    @property
    def _collection(self):
        # TODO: check whether DB name should be configurable
        db = getattr(self._client, self._db_name)
        return db.app_configurations

    async def initialize(self):
        await self._collection.create_index("app_id", unique=True)

    async def add_configuration(self, app_id, configuration):
        document = await self._collection.find_one({"app_id": {"$eq": app_id}})
        if document:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT, detail="App with given id already has a configuration"
            )
        document = dict(app_id=app_id, configuration=configuration)
        result = await self._collection.insert_one(document)
        return result.inserted_id

    async def get_configuration(self, app_id):
        document = await self._collection.find_one({"app_id": {"$eq": app_id}})
        if not document:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No such configuration")
        return document["configuration"]
